/**
 * 
 */
package guiSkeleton.pseudoGUI;

import guiSkeleton.guiEvents.GUIEvent;
import guiSkeleton.guiEvents.GUIEventListener;
import guiSkeleton.guiEvents.GUIEventSource;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;


/**
 * The Concrete Subject class for the Observer Pattern example 
 * @author hogan
 *
 */
public class GUIButton implements GUIEventSource {
	private String name; 
	private boolean clicked;
	private ArrayList<GUIEventListener> guiEventListerners;
	
	public GUIButton(String nm) {
		this.name=nm;
		this.clicked=false; 
		this.guiEventListerners = new ArrayList<GUIEventListener>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void doClick() {
		clicked = true;;
		fireGUIEvent();
		clicked = false;
	}

	@Override
	public void fireGUIEvent() {
		ListIterator<GUIEventListener> iter = this.guiEventListerners.listIterator();
		while (iter.hasNext()) {
			iter.next().guiActionPerformed(new GUIEvent(this));
		}
	}

	@Override
	public void addGUIEventListener(GUIEventListener gel) {
		this.guiEventListerners.add(gel);
	}

	@Override
	public void removeGUIEventListener(GUIEventListener gel) {
		this.guiEventListerners.remove(gel);
	}
	
	@Override
	public String toString() { 
		return "guiButton:" + this.name; 
	}
}
